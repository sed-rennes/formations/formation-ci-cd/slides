
Slides for a general formation about CI/CD (Continuous Integration/Continuous Deployment) in Inria context.

It offers several versions, a generic version and version containing CI specific descriptions.

It currently considers the following CI systems often used at Inria:
- gitlab-ci
- github Action
- Jenkins

It may also consider CDash (if we find a contributor :wink: )

See the artefcts in the following pipeline for the latest versions as pdf:

![Pipeline](https://gitlab.inria.fr/sed-rennes/formations/formation-ci-cd/slides/badges/master/pipeline.svg)


Organistion des cours:
- Session 1 - Présentation principale : (doit permettre de répondre aux questions, de choisir une techno)
    - Qu'est ce qu'une CI ?
        - principe communs
        - offre locale (Jenkins / Gitlab)
    - Quelle CI choisir pour mon projet spécifique ?
        - Les différences dans les grandes lignes
            - Présentations des résultats (qualité des rapports)
            - Gestion des configurations 
            - Gestion des Runners 


- Présentations spécifiques (jenkins, gitlab, github runners) : 
 Pré-requis : comptes et accès au projet exemple, participation à la session 1
    - Quickstart
    - Livre recettes (FAQ)
    - Session 2A Jenkins
    - Session 2B GitlabCI
    - Session 2C Github Actions
    - 

TODO
Eric remonter slide de 30 à 25 sur les Runners