######################################################################
# 
#
######################################################################


help: 
	@echo --------------------------
	@echo make main-full.pdf    : full version \(with gitlab, github, jenkins and cdash specific slides\)
	@echo make main-gitlab.pdf  : version with only gitlab specific slides 
	@echo make main-github.pdf  : version with only github action specific slides
	@echo make main-jenkins.pdf  : version with only jenkins specific slides
	@echo make main.pdf    :  version without any specific slides
	@echo make clean 
	

	
%.pdf: %.tex $(shell find . -name "*.tex")
	@pdflatex -shell-escape $(@:.pdf=)
	
	
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# remove unused files
clean:
	@rm -f *.aux *.blx 
	@rm -f *.idx *.log
	@rm -f *.toc *.blg *.nav *.snm
	@rm -f *.vrb
